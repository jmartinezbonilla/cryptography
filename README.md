# Cryptography training

This training uses jupypter notebooks as a place for explaining the concepts about cryptography. Jupyter allows
for mixing markdown and python code in the same document. Follow these instructions to set up your environment:

Using virtual environments is recommended but is not mandatory. You'll need `virtualenv` to be installed
in your python environment:
```
pip install virtualenv
```

To set up everything:
```
virtualenv .venv
source .venv/bin/activate 

pip install -r requirements
jupyter lab
```

This will launch a Jupyter lab in your browser.