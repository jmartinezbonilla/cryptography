#!/bin/sh

echo "Copying images"
mkdir -p wiki/img
cp -r img/*.png wiki/img

echo "Converting to markdown"
for entry in *.ipynb
do
    echo "Converting $entry to markdown"
    jupyter nbconvert --to markdown --output-dir "wiki/" "$entry"
done

echo "Converting to doc"
for entry in wiki/*.md
do
    echo "Converting $entry to DOCX"
    pandoc --from markdown --to docx -o "$entry.docx" "$entry"
done
