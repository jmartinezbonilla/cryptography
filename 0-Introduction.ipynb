{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Introduction\n",
    "## The REAL problem\n",
    "\n",
    "There are many aspects to cryptography and there are many problems that it tries to solve. When thinking about encrypting messages the first thought is something \n",
    "like this:\n",
    "\n",
    "![Basic problem](img/cryptography-main-problem.png)\n",
    "\n",
    "We have Alice trying to communicate with Bob, and a third person, Charlie, wants to hear the message. While this is one of the foundational issues around why cryptography exists\n",
    "it definitely is not the only issue it tries to solve.\n",
    "\n",
    "* How does Alice know that Bob is actually **the Bob that she knows**?\n",
    "* How does Bob know that the message comes from **the Alice that he knows**?\n",
    "* How does Bob know that the message wasn't **changed in transit**?\n",
    "* How can Bob **prove that Alice sent a message** even if she denies it?\n",
    "\n",
    "Charlie has a wide array of attacks that he can perform toward Alice and/or Bob, and cryptography introduces several mechanisms to protect a communication line\n",
    "from these:\n",
    "\n",
    "![Other problems](img/cryptography-issues.png)\n",
    "\n",
    "\n",
    "To put some fancy names to it, these are the services provided by a cryptographic system:\n",
    "1. **Confidentiality**: the main requirement for it, how to protect unauthorized subjects to access sensitive information.\n",
    "2. **Authentication / Trust**: provide a way to prove that a given person is who they claim to be, on either side of the conversation.\n",
    "3. **Integrity**: verify that the message that is received is exactly the same as the one that was sent.\n",
    "4. **Non-repudiation**: assurance that the sender of a message cannot convincingly deny to have sent it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Elements of a cryptosystem\n",
    "\n",
    "We'll take the Wikipedia definition to say that a cryptosystem is \"a suite of cryptographic algorithms needed to implement a particular security service, such as confidentiality\" ([src](https://en.wikipedia.org/wiki/Cryptosystem)). So, to put this in perspective, is basically the tools that we have at our disposal to have the services discussed in the previous section. The main service is confidentiality, but as we'll see later, some of the same tools can provide for the rest of the services.\n",
    "\n",
    "Now, to have some definitions in place we'll take a look at a basic cryptosystem for communication:\n",
    "\n",
    "![Parts of a cryptosystem](img/cryptography-basic-system.png)\n",
    "\n",
    "In here we can see:\n",
    "* The sender and receivers, who are the subjects involved in the communication.\n",
    "* **Plaintext**: the message in readable format for both the sender and receiver.\n",
    "* **Ciphertext**: the message in scrambled format, should be completely unreadable to anyone that intercepts the message.\n",
    "* **Encryption / Decryption algorithms**: the process to convert between plaintext and ciphertext.\n",
    "* **Encryption / Decryption keys**: the input on each of these algorithms that allows for converting between plaintext and ciphertext (a bit vague, I know, but we'll see more about those in following sections).\n",
    "* **Communication channel**: the medium that the sender and receiver chose to communicate.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Basic principles\n",
    "\n",
    "## The Kerckhoffs' principle\n",
    "\n",
    "Modern cryptography goes through a peer reviewed process that involves highly talented individuals in order to get algorithms that are very hard to break. They all rely on the [Kerckhoffs' principle](https://en.wikipedia.org/wiki/Kerckhoffs%27s_principle):\n",
    "\n",
    "```\n",
    "A cryptosystem should be secure, even if everything about the system, except the key, is public knowledge.\n",
    "```\n",
    "\n",
    "It might come as a surprise that all regarding a cryptosystem should be public knowledge, but it comes with all the interestings perks of open source software. It is a peer reviewed system where all the parties involved have a stake on it working correctly. Plus the best algorightms are the ones that have more active support, while the less useful are deprecated and lose suport."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## The YANAC principle\n",
    "\n",
    "This is one of the most important principles of cryptography: **DON'T TRY TO CREATE YOUR OWN CRYPTOSYSTEM!**\n",
    "\n",
    "It is hard to get it right and way too easy to get it wrong. To put it in context let's see something interesting.\n",
    "This is the Enigma machine, used during World War II by Nazi Germany. It was considered so secure that it was used for top secret messages:\n",
    "\n",
    "![Enigma machine](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Enigma_%28crittografia%29_-_Museo_scienza_e_tecnologia_Milano.jpg/440px-Enigma_%28crittografia%29_-_Museo_scienza_e_tecnologia_Milano.jpg)\n",
    "\n",
    "[This is a good explanation on how it worked](https://static.guim.co.uk/ni/1415981437904/Enigma_Machine_WEB.svg)\n",
    "\n",
    "Now, I'm talking about all this because the Enigma was one of the most clever machines devised for encrypting codes. But it had one mathematical flaw:\n",
    "`no letter would be encrypted as itself` [(src)](https://www.theguardian.com/technology/2014/nov/14/how-did-enigma-machine-work-imitation-game). \n",
    "With that information and knowing a little about the messages that could be sent, **the allied forces manage to break the codes!**\n",
    "\n",
    "Let's see the type of codes that could be generated with this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "9b23dc181b0847da816c443f12c70547",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Text(value='Attack at noon', description='Message:', placeholder='Type something')"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import ipywidgets as widgets\n",
    "\n",
    "message = widgets.Text(\n",
    "    value='Attack at noon',\n",
    "    placeholder='Type something',\n",
    "    description='Message:',\n",
    "    disabled=False\n",
    ")\n",
    "message"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Encryption key: KCH\n",
      "Ciphertext: EVHQZVFZYYUZZX\n"
     ]
    }
   ],
   "source": [
    "from enigma.machine import EnigmaMachine\n",
    "\n",
    "# Build Enigma machine\n",
    "# All the settings regarding rotors, rings, plugboard... are the equivalent of a key\n",
    "# The operator would set these according to the daily settings given in a manual\n",
    "machine = EnigmaMachine.from_key_sheet(\n",
    "       rotors='II IV V',\n",
    "       reflector='B',\n",
    "       ring_settings=[1, 20, 11],\n",
    "       plugboard_settings='AV BS CG DL FU HZ IN KM OW RX')\n",
    "\n",
    "# Then, the operator would select the initial rotor position (WXC) and a three letter message (BLA)\n",
    "machine.set_display('WXC')\n",
    "enc_key = machine.process_text('BLA')\n",
    "print('Encryption key:', enc_key)\n",
    "\n",
    "# Afterwards, the operator would set the rotors to the three letter message (BLA) and encrypt the message using those values\n",
    "machine.set_display('BLA')\n",
    "ciphertext = machine.process_text(message.value, replace_char='X')\n",
    "\n",
    "print('Ciphertext:', ciphertext)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Encryption key for message: BLA\n",
      "Plaintext: ATTACKXATXNOON\n"
     ]
    }
   ],
   "source": [
    "# The message to send then would include at least\n",
    "# - Initial rotor position in unencrypted format (WXC)\n",
    "# - Encrypted rotor message key value (KCH)\n",
    "# - Ciphertext\n",
    "\n",
    "# With these the receiving operator can get the same values with that information\n",
    "machine.set_display('WXC')\n",
    "enc_key = machine.process_text('KCH')\n",
    "print('Encryption key for message:', enc_key)\n",
    "\n",
    "# Finally, would set the rotors to the encryption key given and decrypt the message\n",
    "machine.set_display('BLA')\n",
    "plaintext = machine.process_text(ciphertext)\n",
    "print('Plaintext:', plaintext)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "So... it is a VERY complicated machine, devised by state of the art mathematicians and engineers. And still it was broken by a motivated enough adversary!!!\n",
    "\n",
    "# Moving forward\n",
    "\n",
    "We are going to see how the different cryptographic primitives fit together to give us a full\n",
    "cryptographic system that solves each of the issues discussed before. These are the topics:\n",
    "\n",
    "* [Symmetric cryptography](1-Symmetric-cryptography.ipynb): encryption and decryption using shared keys.\n",
    "* [Asymmetric encryption](2-Asymmetric-cryptography.ipynb): encryption and decryption using different keys.\n",
    "* [Hashing](3-Cryptographic-hash.ipynb): one-way functions for integrity check.\n",
    "* Digital certificates: identity proof via trust chains."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
