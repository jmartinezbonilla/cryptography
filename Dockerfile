FROM jupyter/scipy-notebook:ubuntu-20.04

USER root
# Install pandoc
RUN wget https://github.com/jgm/pandoc/releases/download/2.18/pandoc-2.18-1-amd64.deb -O /tmp/pandoc.deb \
    && dpkg -i /tmp/pandoc.deb \
    && rm /tmp/pandoc.deb

# Install apt-get dependencies
RUN apt-get update && apt-get install -y \
    libgtk-3-0 \
    libnotify4 \
    libxtst6 \
    libatspi2.0-0 \
    libsecret-1-0 \
    xvfb \
    && rm -rf /var/lib/apt/lists/*

# Install draw.io
RUN wget https://github.com/jgraph/drawio-desktop/releases/download/v13.0.3/draw.io-amd64-13.0.3.deb -O /tmp/drawio.deb \
    && dpkg -i /tmp/drawio.deb \
    && rm /tmp/drawio.deb

USER jovyan

# Install nbconvert
RUN conda install -y\
    nbconvert \
    pip

COPY requirements.txt /tmp/requirements.txt

# Install python pip requirements
RUN pip install -r /tmp/requirements.txt 