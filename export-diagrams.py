#!/usr/bin/python3

import xml.etree.ElementTree as etree
import os
import pickle
import tempfile
import subprocess


def export_diagrams(file_name:str, file_path:str, output_dir:str):
    # draw_app = 'drawio'
    draw_app = '/Applications/draw.io.app/Contents/MacOS/draw.io'

    # Export to XML and load data
    with tempfile.NamedTemporaryFile(delete=True) as xml_file:
        args = [draw_app, '--export', '--format', 'xml', '--uncompressed', '--output', xml_file.name, file_path]
        process = subprocess.run(args)
        xml_data = etree.parse(xml_file)
    
    # Get amount of files and their names
    page_id = 0
    for diagram in xml_data.findall('.//diagram'):
        diagram_name = diagram.get('name')
        diagram_filename = '%s-%s.png' % (file_name, diagram_name)

        # Export the diagram
        print('Exporting:', diagram_filename)

        args = [draw_app, '--export', '--page-index', '%d' % page_id, '--transparent', '--output', os.path.join(output_dir, diagram_filename), file_path]
        process = subprocess.run(args)

        page_id += 1
    
if __name__=='__main__':
    diagrams_dir = 'img/diagrams/'
    image_dir = 'img/'

    for file in os.listdir(diagrams_dir):
        if file.endswith('.drawio'):
            export_diagrams(os.path.splitext(file)[0], os.path.join(diagrams_dir, file), image_dir)
